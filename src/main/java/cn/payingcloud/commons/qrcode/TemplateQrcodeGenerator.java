package cn.payingcloud.commons.qrcode;

import cn.payingcloud.commons.qrcode.util.ImageUtils;
import cn.payingcloud.commons.qrcode.util.QrcodeUtils;
import cn.payingcloud.commons.qrcode.util.ZipUtils;
import com.google.zxing.WriterException;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ZM.Wang
 */
public class TemplateQrcodeGenerator {

    private final BufferedImage bufferedImage;
    private final int codeWidth;
    private final int topOffset;

    public TemplateQrcodeGenerator(InputStream templateImgStream, int codeWidth, int topOffset) {
        if (templateImgStream == null) {
            throw new IllegalArgumentException("templateImgStream must not be null");
        }
        this.codeWidth = codeWidth;
        this.topOffset = topOffset;
        try {
            this.bufferedImage = ImageIO.read(templateImgStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public ByteArrayOutputStream getZip(List<String> contents) throws WriterException, IOException {
        List<ByteArrayOutputStream> outputStreams = new ArrayList<>();
        int mark;
        String no;
        for (String url : contents) {
            mark = url.lastIndexOf("/");
            no = url.substring(mark + 1, url.length());
            Image qrCode = QrcodeUtils.createQrcode(url, codeWidth);
            ByteArrayOutputStream outputStream = ImageUtils.overlapImg(qrCode, codeWidth, bufferedImage, topOffset);
            outputStreams.add(outputStream);
        }
        return ZipUtils.createZip(outputStreams);
    }

    public ByteArrayOutputStream getQrcode(String url) throws WriterException, IOException {
        Image qrCode = QrcodeUtils.createQrcode(url, codeWidth);
        return ImageUtils.overlapImg(qrCode, codeWidth, bufferedImage, topOffset);
    }
}
