package cn.payingcloud.commons.qrcode.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author ZM.Wang
 */
public class ImageUtils {

    public static ByteArrayOutputStream overlapImg(Image topImg, int topImgWidth, BufferedImage bottomImg, int topOffset) throws IOException {
        //得到画笔对象
        BufferedImage bufferedImage = new BufferedImage(bottomImg.getWidth(), bottomImg.getHeight(), bottomImg.getType());
        bufferedImage.setData(bottomImg.getData());

        Graphics g = bufferedImage.getGraphics();

        int bufferedImageWidth = bufferedImage.getWidth();

        int topImgLeftOffset = (bufferedImageWidth - topImgWidth) / 2;

        //将小图片绘到大图片上。
        //中间数字表示你的小图片在大图片上的位置。
        g.drawImage(topImg, topImgLeftOffset, topOffset, null);

        g.dispose();

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "png", os);
        return os;
    }
}
