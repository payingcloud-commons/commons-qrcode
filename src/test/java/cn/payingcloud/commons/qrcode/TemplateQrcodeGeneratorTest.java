package cn.payingcloud.commons.qrcode;

import com.google.zxing.WriterException;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;

/**
 * @author ZM.Wang
 */
public class TemplateQrcodeGeneratorTest {


    @Test
    public void testGetQrcode_cashier() throws WriterException, IOException {
        TemplateQrcodeGenerator generator;
        try (InputStream in = this.getClass().getResourceAsStream("/cashier.png")) {
            generator = new TemplateQrcodeGenerator(in, 620, 320);
        }
        ByteArrayOutputStream outputStream = generator.getQrcode("https://m.payingcloud.cn/cashier/5915324e04d0450001ef3edf");
        OutputStream os = new FileOutputStream("d:/image.png");
        os.write(outputStream.toByteArray());
        Assert.assertNotNull(outputStream);
    }

    @Test
    public void testGetQrcode_promotion() throws WriterException, IOException {
        TemplateQrcodeGenerator generator;
        try (InputStream in = this.getClass().getResourceAsStream("/promotion.png")) {
            generator = new TemplateQrcodeGenerator(in, 620, 320);
        }
        ByteArrayOutputStream outputStream = generator.getQrcode("https://m.payingcloud.cn/promotion/591575c604d045000191ea2b");
        OutputStream os = new FileOutputStream("d:/591575c604d045000191ea2b.png");
        os.write(outputStream.toByteArray());
        Assert.assertNotNull(outputStream);
    }
}
